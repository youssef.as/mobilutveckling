import { NavigationContainer } from "@react-navigation/native"
import { StyleSheet, Text, View } from 'react-native';
import Login from './components/Login';
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import Home from "./screens/Home";
import { useState } from "react";
import GreyWall from "./components/GreyWallComp";

export default function App() {

  const StackNavigation = createNativeStackNavigator();
  const Tab = createNativeStackNavigator();

  const Test = () => {
    <GreyWall />
  }

    return (
        <NavigationContainer>
            <StackNavigation.Navigator>
              <StackNavigation.Screen
                options={{headerShown: false}}
                name='Login'
                component={Login}
              />
              <StackNavigation.Screen
                options={{headerShown: false}}
                name='Home'
                component={Home}
              />
              {/* <StackNavigation.Screen
                options={{headerShown: false}}
                name='Test'
                component={Test}
              /> */}
            </StackNavigation.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }, heads: {
    color: 'black',
    fontSize: 15
  }
});
