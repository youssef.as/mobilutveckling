import { createNativeStackNavigator } from "@react-navigation/native-stack"
import AdminHome from "../components/AdminHomeComp";

const UsersScreen = ({appUser}) => {

    const UsersNavigation = createNativeStackNavigator();

    const TempAdminHome = () => {
        return (
            <AdminHome appUser={appUser} />
        )
    }

    return (
            <UsersNavigation.Navigator>
                <UsersNavigation.Screen
                    options={{headerShown: false}}
                    name='AdminHomeComp'
                    component={TempAdminHome}
                />
            </UsersNavigation.Navigator>
    );
}

export default UsersScreen;