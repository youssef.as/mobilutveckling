import { useEffect, useState } from "react";
import MainBottomComp from "../components/MainBottomComp";
import AppUser from "../models/AppUser";


const Home = ({ route }) => {
    const [appUser, setAppUser] = useState({});

    const {email, id, roles, token, type, username} = route.params.res;

    useEffect (() => {
        setAppUser(new AppUser(email, id, roles, token, type, username));
    }, []);

    return (
        <MainBottomComp appUser={appUser} />
    )
}

export default Home;