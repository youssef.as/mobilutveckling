import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { FontAwesome } from '@expo/vector-icons';
import GreyWallComp from "../components/GreyWallComp";
import UserWallComp from "./UserWallComp";
import AdminHomeComp from "./AdminHomeComp";
import { useEffect, useState } from "react";
import AddUserComp from "./AddUserComp";

const MainBottomComp = ({appUser}) => {
    const [roles, setRoles] = useState([]);
    const [admin, setAdmin] = useState(false);
    const [mod, setMod] = useState(false);
    const [user, setUser] = useState(false);

    const BottomTab = createBottomTabNavigator();

    useEffect(() => {
        setRoles(appUser.roles);

        if (appUser != null && roles != undefined) {
            if (roles.includes("ROLE_ADMIN")) setAdmin(true);
            if (roles.includes("ROLE_MODERATOR")) setMod(true);
        }
    }, [roles])

    const UserWall = () => {
        return (
            <UserWallComp appUser={appUser} />
        )
    }

    const GreyWall = () => {
        return (
            <GreyWallComp appUser={appUser} />
        )
    }

    const AdminHome = () => {
        return (
            <AdminHomeComp appUser={appUser} />
        )
    }

    const AddUser = () => {
        return (
            <AddUserComp appUser={appUser} />
        )
    }

    if (admin) {
        return (
            <BottomTab.Navigator screenOptions={{ tabBarShowLabel: false, headerShown: false }}>
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='user' size={24} color='black' />}}
                    name='UserWall' 
                    component={UserWall} 
                />
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='book' size={24} color='black' />}}
                    name='GreyWall' 
                    component={GreyWall}
                />
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='users' size={24} color='gold' />}}
                    name='AdminHome' 
                    component={AdminHome}
                />
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='users' size={24} color='green' />}}
                    name='AddUser' 
                    component={AddUser}
                />
            </BottomTab.Navigator>
        )
    }

    if (mod) {
        return (
            <BottomTab.Navigator screenOptions={{ tabBarShowLabel: false, headerShown: false }}>
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='user' size={24} color='black' />}}
                    name='UserWall' 
                    component={UserWall} 
                />
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='book' size={24} color='black' />}}
                    name='GreyWall' 
                    component={GreyWall}
                />
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='users' size={24} color='silver' />}}
                    name='AdminHome' 
                    component={AdminHome}
                />
            </BottomTab.Navigator>
        )
    }

    return (
        <BottomTab.Navigator screenOptions={{ tabBarShowLabel: false, headerShown: false }}>
            <BottomTab.Screen 
                options={{tabBarIcon: () => <FontAwesome name='user' size={24} color='black' />}}
                name='UserWall' 
                component={UserWall} 
            />
            <BottomTab.Screen 
                options={{tabBarIcon: () => <FontAwesome name='book' size={24} color='black' />}}
                name='GreyWall' 
                component={GreyWall}
            />
        </BottomTab.Navigator>
    )

}

export default MainBottomComp