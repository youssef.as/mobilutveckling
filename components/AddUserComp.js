import { useState } from "react";
import { StyleSheet, Text, View, TextInput, Pressable } from "react-native";
import SelectDropdown from 'react-native-select-dropdown'

const AddUserComp = ({appUser}) => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState([]);
    const diffRoles = ["User", "Moderator", "Administrator"]

    const handleUsernameChange = (input) => {
        setUsername(input);
    }

    const handleEmailChange = (input) => {
        setEmail(input);
    }

    const handlePasswordChange = (input) => {
        setPassword(input);
    }

    const handleAddUser = () => {
        console.log(role)
        var headers = {
            "Content-Type": "application/json",                                                                                                
            "Access-Control-Origin": "*"
         }

        var data = {
            "username": username,
            "email": email,
            "password": password,
            "role": role
        }

        fetch("http://10.0.2.2:8080/api/auth/signup", {
            method: "POST",
            headers: headers,
            body:  JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                setUsername('')
                setEmail('')
                setPassword('')
            })
            .catch(err => console.log(err))
    }

    return (
        <View style={styles.container}>
            <Text>Add a new user</Text>
            <Text>Username</Text>
            <TextInput
                style={styles.input}
                onChangeText={handleUsernameChange}
                value={username}
            />

            <Text>Email</Text>
            <TextInput
                style={styles.input}
                onChangeText={handleEmailChange}
                value={email}
            />

            <Text>Password</Text>
            <TextInput
                style={styles.input}
                onChangeText={handlePasswordChange}
                value={password}
            />

            <SelectDropdown
                data={diffRoles}
                onSelect={(selectedItem, index) => {
                    if(index == 2) {
                        setRole(["user", "mod", "admin"])
                    }
                    if(index == 1) {
                        setRole(["user", "mod"])
                    }
                    if(index == 0) {
                        setRole(["user"])
                    }
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />

            <Pressable
                style={styles.press}
                onPress={handleAddUser}
            >
                <Text
                    style={styles.log}
                >Add</Text>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }, check: {
        width: 100,
        height: 100,
        backgroundColor: 'black'
    }, input: {
        backgroundColor: '#eee',
        minWidth: '60%',
        borderRadius: 2.5,
        paddingHorizontal: 5,
        paddingVertical: 2,
    }, press: {
        backgroundColor: '#0699FF',
        marginVertical: 10,
        borderRadius: 2.5,
        paddingHorizontal: 5,
        paddingVertical: 2
    }, log: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20
    }
});

export default AddUserComp;