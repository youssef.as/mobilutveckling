import { useNavigation } from "@react-navigation/native";
import { useState } from "react";
import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";

const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    // const [appUser, setAppUser] = useState({});

    const nav = useNavigation();

    const handleUsernameChange = (input) => {
        setUsername(input);
    }

    const handlePasswordChange = (input) => {
        setPassword(input);
    }

    const handleLogin = () => {
        var headers = {
            "Content-Type": "application/json",                                                                                                
            "Access-Control-Origin": "*"
         }

        var data = {
            "username": username,
            "password": password
        }

        fetch("http://10.0.2.2:8080/api/auth/signin", {
            method: "POST",
            headers: headers,
            body:  JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                nav.navigate('Home', {res: res})
            })
            .catch(err => console.log(err))
    }

    return (
        <View style={styles.cent}>
            <View style={styles.login}>
                <Text>Username</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={handleUsernameChange}
                    value={username}
                />
                <Text>Password</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={handlePasswordChange}
                    value={password}
                    secureTextEntry={true}
                />
                <Pressable
                    style={styles.press}
                    onPress={handleLogin}
                >
                    <Text style={styles.log}>
                        Login
                    </Text>
                </Pressable>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    cent: {
        flex: 1,
        alignItems: 'center'
    }, login: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    }, input: {
        backgroundColor: '#eee',
        minWidth: '60%',
        borderRadius: 2.5,
        paddingHorizontal: 5,
        paddingVertical: 2,
    }, press: {
        backgroundColor: '#0699FF',
        marginVertical: 10,
        borderRadius: 2.5,
        paddingHorizontal: 5,
        paddingVertical: 2
    }, log: {
        textAlign: 'center',
        color: 'white'
    }
});

export default Login;