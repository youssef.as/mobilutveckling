import { useEffect, useState } from "react";
import { FlatList, Pressable, StyleSheet, Text, View } from "react-native";

const AdminHomeComp = ({appUser}) => {
    const [loading, setLoading] = useState(false);
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState({});
    const [token, setToken] = useState('');
    // const {email, id, roles, token, type, username} = appUser;

    const handlePress = () => {

    }

    const _renderItem = ({item}) => {
        // console.log(item);
        if (item.username != "admin" && item.username != "mod") {
            if (user.roles.includes("ROLE_ADMIN")) {
                return (
                    <View style={styles.card}>
                        <Pressable
                            onPress={handlePress}
                        >
                            <View>
                                <Text>ID: {item.id}</Text>
                                <Text style={styles.userHead}>{item.username}</Text>
                                <Text>{item.email}</Text>
                            </View>
                        </Pressable>
                        <Pressable
                            onPress={() => {
                                const tokenStr = token.substring(1, token.length-1);
                                fetch("http://10.0.2.2:8080/api/test/deleteUser/" + item.id, {
                                    method: "POST",
                                        headers: {
                                            'Content-type': 'application/json',
                                            'Authorization': `Bearer ${tokenStr}`
                                        }
                                })
                                    .then(res => res.json())
                                    .then(res => {
                                        setUsers(res);
                                    })
                                    .catch(err => console.log(err))
                            }}
                        >
                            <Text>Delete</Text>
                        </Pressable>
                    </View>
                )
            }
            return (
                <View style={styles.card}>
                        <Pressable
                            onPress={handlePress}
                        >
                            <View>
                                <Text>ID: {item.id}</Text>
                                <Text style={styles.userHead}>{item.username}</Text>
                                <Text>{item.email}</Text>
                            </View>
                        </Pressable>
                    </View>
            )
        }
    }

    useEffect(() => {
        setLoading(true);
        // console.log(appUser);
        setUser(appUser);
        setToken(JSON.stringify(appUser.token));

        if (token.length > 0) {
            const tokenStr = token.substring(1, token.length-1);

            // console.log(bearer)
            fetch("http://10.0.2.2:8080/api/test/findAllUsers", {
                method: "GET",
                headers: {
                    'Content-type': 'application/json',
                    'Authorization': `Bearer ${tokenStr}`
                }
            })
                .then(res => res.json())
                .then(res => {
                    setUsers(res);
                })
                .catch(err => console.log(err))
        }
        setLoading(false);
    }, [users, user, token])


    return (
        <View style={styles.allCards}>
            <Text style={styles.header}>Application Users:</Text>
            <FlatList
                data={users}
                renderItem={_renderItem}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    allCards: {
        marginBottom: 100
    },
    header: {
        fontSize: 30,
        marginTop: 30,
        marginBottom: 15,
        marginHorizontal: 10
    },
    card: {
        flex: 1,
        margin: 10,
        padding: 20,
        borderRadius: 15,
        backgroundColor: 'silver',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    userHead: {
        fontSize: 20,

    }
});

export default AdminHomeComp;