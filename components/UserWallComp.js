import { useNavigation } from "@react-navigation/native";
import { FlatList, Pressable, StyleSheet, Text, View } from "react-native";

const UserWall = ({appUser}) => {
    const {email, id, roles, token, type, username} = appUser;

    const nav = useNavigation();

    const _renderItem = ({item}) => {
        if (item == "ROLE_ADMIN") {
            item = "ADMIN";
            return (
                <Text style={styles.adminPin}>{item}</Text>
            )
        }
        if (item == "ROLE_MODERATOR") {
            item = "MODERATOR";
            return (
                <Text style={styles.modPin}>{item}</Text>
            )
        }

        item = "USER";
        return (
            <Text style={styles.userPin}>{item}</Text>
        )
    }

    const handleLogout = () => {
        nav.navigate('Login')
    }

    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>{username}</Text>
            <Text>{email}</Text>
            <Text>ID: {id}</Text>
            <Text style={styles.rolePins}>
                <FlatList
                    data={roles}
                    renderItem={_renderItem}
                />
            </Text>

            <Pressable
                style={styles.press}
                onPress={handleLogout}
            >
                <Text
                    style={styles.log}
                >
                    Log Out
                </Text>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    press: {
        backgroundColor: '#0699FF',
        marginVertical: 15,
        borderRadius: 2.5,
        paddingHorizontal: 5,
        paddingVertical: 2
    }, log: {
        textAlign: 'center',
        color: 'white'
    }, rolePins: {
        marginTop: 10,
    }, centerPins: {
        textAlign: 'center'
    }, adminPin: {
        backgroundColor: 'gold',
        borderRadius: 5,
        padding: 5,
        color: 'white',
        textAlign: 'center'
    }, modPin: {
        backgroundColor: 'silver',
        borderRadius: 5,
        padding: 5,
        color: 'white',
        textAlign: 'center'
    }, userPin: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 5,
        textAlign: 'center'
    }
});

export default UserWall;