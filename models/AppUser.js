export default class AppUser {
    constructor(
            email,
            id,
            roles,
            token,
            type,
            username
        ) {
            this.email = email;
            this.id = id;
            this.roles = roles;
            this.token = token;
            this.type = type;
            this.username = username;
    }
}