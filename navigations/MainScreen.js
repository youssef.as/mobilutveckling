import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { NavigationContainer } from "@react-navigation/native"
import { FontAwesome } from '@expo/vector-icons';

const MainScreen = () => {

    const BottomTab = createBottomTabNavigator();

    const About = () => {
        return (
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>
              About Screen!
            </Text>
          </View>
        )
      }

    return (
        <NavigationContainer>
            <BottomTab.Navigator screenOptions={{ tabBarShowLabel: false, headerShown: false }}>
                <BottomTab.Screen
                    options={{tabBarIcon: () => <FontAwesome name='tasks' size={24} color='black' />}}
                    name='MainStack' 
                    component={MainStack}
                />
                <BottomTab.Screen 
                    options={{tabBarIcon: () => <FontAwesome name='book' size={24} color='black' />}}
                    name='About' 
                    component={About} 
                />
            </BottomTab.Navigator>
        </NavigationContainer>
    )
}

export default MainScreen;